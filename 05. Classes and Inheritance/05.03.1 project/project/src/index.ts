class Person {
  name = "John";
  email = "john@email.com";

  greet() {
    return `Hello John`;
  }
}
