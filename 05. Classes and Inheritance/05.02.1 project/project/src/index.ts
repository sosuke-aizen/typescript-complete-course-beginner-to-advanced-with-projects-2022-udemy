let person = {
  name: "John",
  email: "john@email.com",
  greet: () => console.log(`Hello ${person.name}`),
};
