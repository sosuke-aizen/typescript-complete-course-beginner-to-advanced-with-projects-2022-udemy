const book = {
  title: 'the title',
  pages: 300,
  author: 'John',
};

const book2 = new Object();
book2.title = 'Book2 title';
book2.pages = 250;
book2.author = 'Mark';

console.log(book2);
