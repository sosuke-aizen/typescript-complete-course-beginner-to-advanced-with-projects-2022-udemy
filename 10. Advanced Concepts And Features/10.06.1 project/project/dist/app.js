"use strict";
function nextDayForAWeekDay(weekday) {
    switch (weekday) {
        case 'Mon':
            return 'Tue';
        case 'Tue':
            return 'Wed';
        case 'Wed':
            return 'Thu';
        case 'Thu':
            return 'Fri';
        case 'Fri':
            return 'Sat';
    }
}
