"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var Manufacturers;
(function (Manufacturers) {
    Manufacturers["boeing"] = "boeing";
    Manufacturers["airbus"] = "airbus";
})(Manufacturers || (Manufacturers = {}));
function AircraftManufacturer(manufacturer) {
    return (target) => {
        if (manufacturer === Manufacturers.airbus) {
            target.prototype.origin = 'United States Of America';
            target.prototype.manufacturer = Manufacturers.airbus;
            target.prototype.type = 'Jet';
            target.prototype.airbusMethod = () => {
                console.log('Function performed by Airbus');
            };
        }
        else {
            target.prototype.origin = 'France';
            target.prototype.manufacturer = Manufacturers.boeing;
            target.prototype.type = 'Helicopter';
            target.prototype.boeingMethod = () => {
                console.log('Function performed by Boeing');
            };
        }
    };
}
let Airplane = class Airplane {
    constructor(_aircraftModel, pilot) {
        this._aircraftModel = _aircraftModel;
        this.pilot = pilot;
        console.log('Aircraft Class Instantiated');
    }
    pilotName() {
        console.log(this.pilot);
    }
    get aircraftModel() {
        return this._aircraftModel;
    }
};
Airplane = __decorate([
    AircraftManufacturer(Manufacturers.airbus),
    __metadata("design:paramtypes", [String, String])
], Airplane);
const airplane = new Airplane('Airbus A380', 'John');
console.log(airplane.manufacturer);
airplane.airbusMethod
    ? airplane.airbusMethod()
    : console.log('Method Does not Exist');
