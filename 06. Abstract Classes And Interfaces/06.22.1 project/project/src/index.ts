enum Manufacturer {
  boeing = "boeing",
  airbus = "airbus",
}

function AircraftManufacturer(manufacturer: Manufacturer) {
  return (target: Function) => {
    if (manufacturer === Manufacturer.airbus) {
      target.prototype.origin = "United States Of America";
      target.prototype.manufacturer = Manufacturer.airbus;
      target.prototype.type = "Jet";
    } else {
      target.prototype.origin = "France";
      target.prototype.manufacturer = Manufacturer.boeing;
      target.prototype.type = "Heliopter";
    }
  };
}

interface AircraftInterface {
  _aircraftModel: string;
  prototype?: any;
  origin?: string;
  manufacturer?: string;
  type?: string;
}

@AircraftManufacturer(Manufacturer.airbus)
class Airplane implements AircraftInterface {
  constructor(
    public _aircraftModel: string,
    private pilot: string
  ) {}

  public pilotName() {
    console.log(this.pilot);
  }

  public get aircraftModel() {
    return this._aircraftModel;
  }
}

const airplane: AircraftInterface = new Airplane(
  "A380",
  "John"
);

console.log(airplane.manufacturer);

let x = "y";
let y = 78;
var c = false;
const z = {
  x: 14,
};
const b = "john";

enum Roles {
  admin = "admin",
  author = "author",
}

let user = Roles.admin;
const admin = Roles.admin;

type Weekdays = "Mon" | "Tue" | "Wed" | "Thu" | "Fri";
type Day = Weekdays | "Sat" | "Sunday";

function findNextDay(day: Day) {
  switch (day) {
    case "Mon":
      // code block
      break;
  }
}
